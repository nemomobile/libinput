Name:       libinput
Summary:    Input device library
Version:    1.13.2
Release:    1
Group:      System/Libraries
License:    MIT
URL:        http://www.freedesktop.org/wiki/Software/libinput/
Source0:    %{name}-%{version}.tar.bz2

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(mtdev)
BuildRequires:  pkgconfig(libevdev)
BuildRequires:  meson cmake pkgconfig

%description
libinput is a library that handles input devices for display servers and other
applications that need to directly deal with input devices.

It provides device detection, device handling, input device event processing
and abstraction so minimize the amount of custom input code the user of
libinput need to provide the common set of functionality that users expect.

%package devel
Summary:    Development files for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains the files necessary to develop applications |
that use %{name}.

%prep
%setup -q -n %{name}-%{version}/upstream

%build
%meson \
    -Dlibwacom=false \
    -Ddocumentation=false \
    -Ddebug-gui=false \
    -Dtests=false

%meson_build

%install
%meson_install

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%{_libdir}/libinput.so.*
%{_libdir}/udev/
%{_libexecdir}/%{name}
%{_datadir}/%{name}/

%files devel
%defattr(-,root,root,-)
%{_includedir}/libinput.h
%{_libdir}/libinput.so
%{_libdir}/pkgconfig/libinput.pc
%{_mandir}/man1/*
